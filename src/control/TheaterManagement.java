package control;
import view.GuiTheater;
import model.SeatPrice;


public class TheaterManagement {
	private SeatPrice seatPrice;
	private GuiTheater gui;

	
	public TheaterManagement(SeatPrice seat, GuiTheater gui){
		this.seatPrice = seat;
		this.gui=gui;
		gui.setController(this);
	}
	
	
	public double[][] getSeatPrice(){
		return seatPrice.getSeatPrice();
	}
	
	

}
