package view;
import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import control.TheaterManagement;





public class GuiTheater {
	private TheaterManagement controller;
	private String str = "ABCDEFGHILMNOPQ";
	private double totalPrice;
	private boolean isFirstClick = true;
	private JButton btn;
	private int i;
	private int j;

	
	public void setController(TheaterManagement controller){
		this.controller = controller;

	}
	
	public GuiTheater(){
		JFrame frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setSize(500, 100);
		frame.setTitle("THEATER");
		frame.setLayout(null);

		JLabel timeLabel = new JLabel("Select Time");
		timeLabel.setBounds(15, 20, 80, 30);
		frame.add(timeLabel);
		
		JComboBox timeList = new JComboBox();
		timeList.setBounds(95, 20, 200, 30);
		timeList.addItem("Monday Teater1");
		timeList.addItem("Monday Teater2");
		timeList.addItem("Tuesday Teater1");
		timeList.addItem("Tuesday Teater2");
		timeList.addItem("Wednesday Teater1");
		timeList.addItem("Wednesday Teater2");
		timeList.addItem("Thursday Teater1");
		timeList.addItem("Thursday Teater2");
		timeList.addItem("Friday Teater1");
		timeList.addItem("Friday Teater2");
		timeList.addItem("Saturday Teater1");
		timeList.addItem("Saturday Teater2");
		timeList.addItem("Sunday Teater1");
		timeList.addItem("Sunday Teater2");
		timeList.setSelectedIndex(0);
		frame.add(timeList);
		
		JButton selectTime2 = new JButton("Exit");
		selectTime2.setBounds(400, 20, 80, 30);
		selectTime2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(0);
			}
		});
		frame.add(selectTime2);
		
		JButton selectTime = new JButton("Select");
		selectTime.setBounds(310, 20, 80, 30);
		selectTime.addActionListener(new ActionListener() {
			
			public void actionPerformed(ActionEvent e) {
				// TODO Auto-generated method stub
				
				JFrame frame2 = new JFrame();
				frame2.setResizable(false);
				frame2.setSize(1250, 680);
				frame2.setTitle("THEATER");
				frame2.setLayout(null);
				
				JLabel[] columnLabel = new JLabel[20];
				for(int i = 0; i < 20; i++)
				{
					columnLabel[i] = new JLabel("" + (i+1));
					columnLabel[i].setBounds(100 + (i * 56), 30, 40, 24);
					columnLabel[i].setForeground(new Color(0 ,0 ,139));
					frame2.add(columnLabel[i]);
				}
				
				
				JLabel[] rowLabel = new JLabel[15];
				for(int i = 0; i < 13; i++) {
					rowLabel[i] = new JLabel("" + String.valueOf(str.charAt(i)));
					rowLabel[i].setBounds(25, 80 + (i * 35), 40, 40);
					rowLabel[i].setForeground(new Color(0 ,0 ,139));
					rowLabel[i].setHorizontalAlignment(JTextField.CENTER);
					frame2.add(rowLabel[i]);
				}

				JButton[][] seatBtn = new JButton[13][20];
				for(i = 0; i < 13; i++) {
					for(j=0; j < 20; j++) {
						seatBtn[i][j] = new JButton("" + (int)controller.getSeatPrice()[i][j]);
						seatBtn[i][j].setBounds(80 + (j * 56), 85 + (i * 35), 50, 33);
						seatBtn[i][j].setForeground(new Color(0 ,0 ,139));
						seatBtn[i][j].setBackground(new Color(142 ,229 ,238));
						seatBtn[i][j].setHorizontalAlignment(JTextField.CENTER);
						seatBtn[i][j].putClientProperty("column", i);
						seatBtn[i][j].putClientProperty("row", j);
						seatBtn[i][j].addActionListener(new ActionListener() {
							
							public void actionPerformed(ActionEvent e) {
								// TODO Auto-generated method stub
							    btn = (JButton) e.getSource();
								if (seatBtn[(int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")].getBackground() != new Color(224 ,255 ,255)){
									  
							        if (seatBtn[(int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")].isEnabled()) {
							        	seatBtn[(int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")].setBackground(new Color(224 ,255 ,255));
							        	totalPrice += controller.getSeatPrice()[ (int) btn.getClientProperty("column")][ (int) btn.getClientProperty("row")];
							        }
								}


							}
						});
						
		
						if(controller.getSeatPrice()[i][j] != -1.0) {
							frame2.add(seatBtn[i][j]);
						}
					}
				}
				
				JLabel priceLabel = new JLabel("Total Price :");
				priceLabel.setBounds(760, 580, 80, 30);
				priceLabel.setForeground(new Color(0 ,0 ,139));
				priceLabel.setHorizontalAlignment(JTextField.CENTER);
				frame2.add(priceLabel);

				JTextField priceField = new JTextField();
				priceField.setBounds(850, 580, 75, 30);
				priceField.setHorizontalAlignment(JTextField.CENTER);
				priceField.setText(""+totalPrice);
				frame2.add(priceField);
				
				JButton submit = new JButton("Submit");
				submit.setBounds(960, 580, 75, 30);
				submit.setHorizontalAlignment(JTextField.CENTER);
				frame2.add(submit);
				submit.addActionListener(new ActionListener() {
					
					@Override
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						priceField.setText(""+totalPrice);
					}
				});
				
				JLabel price = new JLabel("Price :");
				price.setBounds(100 ,580 ,100 ,30);
				price.setForeground(new Color(0 ,0 ,139));
				frame2.add(price);
				
				JComboBox priceList = new JComboBox();
				priceList.setBounds(160, 580, 50, 30);
				priceList.addItem("10");
				priceList.addItem("20");
				priceList.addItem("30");
				priceList.addItem("40");
				priceList.addItem("50");
				frame2.add(priceList);
				
				JButton searchPrice = new JButton("Find");
				searchPrice.setBounds(240, 580, 75, 30);
				searchPrice.setHorizontalAlignment(JTextField.CENTER);
				frame2.add(searchPrice);
				searchPrice.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						for(i=0; i < 13; i++) {
							for(j=0; j < 20; j++) {
								if((int)controller.getSeatPrice()[i][j]==Integer.parseInt((String) priceList.getSelectedItem())||(seatBtn[i][j].getBackground() == (new Color(142 ,229 ,238)))){
									seatBtn[i][j].setBackground(new Color(122 ,197 ,205));
								
								}
								
								
							}
						}	
					}
							
				});
				
				JTextField columnField = new JTextField(); 
				JTextField rowField = new JTextField(); 
				JLabel column = new JLabel("Column :");
				JLabel row = new JLabel("Row :");
				column.setBounds(360 ,580 ,50 ,30);
				column.setForeground(new Color(0 ,0 ,139));
				columnField.setBounds(420, 580, 50, 30);
				row.setBounds(495, 580, 50, 30);
				row.setForeground(new Color(0 ,0 ,139));
				rowField.setBounds(535 ,580 ,50 ,30);
				frame2.add(column);
				frame2.add(row);
				frame2.add(rowField);
				frame2.add(columnField);
				
				JButton searchCR = new JButton("Buy");
				searchCR.setBounds(620, 580, 80, 30);
				frame2.add(searchCR);
				searchCR.addActionListener(new ActionListener() {
					
					public void actionPerformed(ActionEvent e) {
						// TODO Auto-generated method stub
						int i=str.indexOf(columnField.getText());
						int j=(Integer.parseInt(rowField.getText())-1);
						System.out.println(""+i);
						System.out.println(""+j);
						if (seatBtn[i][j].getBackground() == new Color(224 ,255 ,255)) {
							JFrame frame3 = new JFrame();
							frame3.setResizable(false);
							frame3.setBounds(450, 250, 400, 200);
							frame3.setTitle("Warning");
							frame3.setLayout(null);
							JLabel caution = new JLabel("This seat is already taken");
							caution.setBounds(120,60,300,30);
							frame3.add(caution);
							JLabel caution2 = new JLabel("Please select other seat");
							caution2.setBounds(125,80,300,30);
							frame3.add(caution2);
							
							frame3.setVisible(true);
						}
						else{
							seatBtn[i][j].setBackground(new Color(224 ,255 ,255));
							totalPrice += controller.getSeatPrice()[i][j];
							priceField.setText(""+totalPrice);
							}
					}
				});
				
				JButton canaelPrice = new JButton("Cancel");
				canaelPrice.setBounds(1070, 580, 75, 30);
				canaelPrice.setHorizontalAlignment(JTextField.CENTER);
				frame2.add(canaelPrice);
				canaelPrice.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});
			
				
				frame2.setVisible(true);

			}
			
		});
		
		
	
		frame.add(selectTime);
		frame.setVisible(true);
	}
}
